﻿using System;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Labs
{
    class XmlSerialization : ISerialization
    {
        public string serialization<T>(T output)
        {
            var xmlSettings = new XmlWriterSettings {OmitXmlDeclaration = true};

            var xmlSerializerNamespace = new XmlSerializerNamespaces();
            xmlSerializerNamespace.Add("", "");
            var stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter, xmlSettings))
            {
                new XmlSerializer(typeof(T)).Serialize(writer, output, xmlSerializerNamespace);
            }
            return stringWriter.ToString();
        }

        public T deserialization<T>(string obj)
        {
            var reader = XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(obj)));
            return (T)new XmlSerializer(typeof(Input)).Deserialize(reader);
        }

        public ISerialization hiddenVerification(string type)
        {
            throw new NotImplementedException();
        }
    }
}
