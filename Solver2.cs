﻿using System;

namespace Labs
{
    class Solver2
    {
        public void solve()
        {
            var port = Console.ReadLine();
            var client = new Client("http://127.0.0.1", port);
            while (!client.Ping()) ;

            var serializedInput = string.Empty;
            while ((serializedInput = client.GetInputData()).Length == 0) ;

            var serializer = new JsonSerialization();
            var input = serializer.deserialization<Input>(serializedInput);
            var output = new Calculator().GenerateOutput(input);
            var serializedOutput = serializer.serialization(output);

            while (!client.WriteAnswer(serializedOutput)) ;
        }
    }
}
