﻿using System;

namespace Labs
{
    class Solver3
    {
        public void solve()
        {
            var port = Console.ReadLine();
            new Server(string.Format("http://127.0.0.1:{0}/", port));
        }
    }
}
