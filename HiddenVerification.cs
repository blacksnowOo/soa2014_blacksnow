﻿namespace Labs
{
    public class HiddenVerification : ISerialization
    {
        public string serialization<T>(T output)
        {
            throw new System.NotImplementedException();
        }

        public T deserialization<T>(string input)
        {
            throw new System.NotImplementedException();
        }

        public ISerialization hiddenVerification(string type)
        {
            switch (type)
            {
                case "Json":
                    return new JsonSerialization();
                case "Xml":
                    return new XmlSerialization();
                default:
                    return null;
            }
        }   
    }
}