﻿using System;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace Labs
{
    //todo: спрятать в базоыве классы методы postInputData, sendResponse
    public abstract class HidenMethods
    {
        private HttpListenerContext context;
        private string requestBody;

        public abstract void getAnswer();
        
        public void postInputData()
        {
            var stream = context.Request.InputStream;
            var encoding = context.Request.ContentEncoding;
            using (var reader = new StreamReader(stream, encoding))
            {
                requestBody = reader.ReadToEnd();
            }
            getAnswer();
        }

        public void sendResponse(string body = "")
        {
            var response = context.Response;
            response.StatusCode = (int) HttpStatusCode.OK;
            response.ContentEncoding = Encoding.UTF8;
            response.ContentLength64 = Encoding.UTF8.GetByteCount(body);
            using (Stream stream = response.OutputStream)
            {
                stream.Write(Encoding.UTF8.GetBytes(body), 0, (int) response.ContentLength64);
            }
        }
    }

    class Server : HidenMethods
    {
        private HttpListener listener;
        private string serverUri;
        private HttpListenerContext context;
        string requestBody;

        public Server(string uri)
        {
            serverUri = uri;
            listener = new HttpListener();
            listener.Prefixes.Add(uri);
            listener.Start();
            requestBody = string.Empty;

            while (listener.IsListening)
            {
                context = listener.GetContext();
                var methodName = context.Request.Url.LocalPath.Substring(1).ToLower();
                var methodInfo = typeof(Server).GetMethods(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)
                    .FirstOrDefault(a => string.Compare(a.Name, methodName, true) == 0);
                if (methodInfo != null)
                    methodInfo.Invoke(this, new Type[] { });
                else
                    sendResponse();
            }
        }

        private void ping()
        {
            sendResponse();
        }

        private void stop()
        {
            sendResponse();
            listener.Stop();
        }

        public override void getAnswer()
        {
            sendResponse(new ServerCalculator().calculateAnswer(requestBody));
        }
    }
}
