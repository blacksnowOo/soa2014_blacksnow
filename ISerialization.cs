﻿namespace Labs
{
    public interface ISerialization
    {
        string serialization<T>(T output);
        T deserialization<T>(string input);
        ISerialization hiddenVerification(string type);
    }
}
