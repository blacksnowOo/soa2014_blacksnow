﻿using Newtonsoft.Json;

namespace Labs
{
    class JsonSerialization : ISerialization
    {
        public string serialization<T>(T output)
        {
            return JsonConvert.SerializeObject(output);
        }

        public T deserialization<T>(string obj)
        {
            return JsonConvert.DeserializeObject<T>(obj);
        }

        public ISerialization hiddenVerification(string type)
        {
            throw new System.NotImplementedException();
        }
    }
}
