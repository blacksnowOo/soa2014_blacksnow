﻿using System.Text;
using System.Net;
using System.IO;

namespace Labs
{
    class Client : IClient
    {
        private string urlServer;
        private string port;
        public Client(string urlServer, string port)
        {
            this.urlServer = urlServer;
            this.port = port;
        }

        public bool WriteAnswer(string serializedOutput)
        {
            var response = (HttpWebResponse)getResponse("writeAnswer", serializedOutput);
            return response != null;
        }

        public string GetInputData()
        {
            var response = (HttpWebResponse)getResponse("getInputData");
            return (response != null) ? new StreamReader(response.GetResponseStream(), Encoding.UTF8).ReadToEnd() : string.Empty;
        }

        public bool Ping()
        {
            var response = (HttpWebResponse)getResponse("ping");
            return (response != null) ? response.StatusCode == HttpStatusCode.OK : false;
        }

        //todo: возвращать не нулл, а нормальный response в случае исключения
        private HttpWebResponse getResponse(string methodName, string body = "")
        {
            var request = WebRequest.Create(string.Format("{0}:{1}/{2}", urlServer, port, methodName));
            request.Timeout = 150;
            request.ContentLength = Encoding.UTF8.GetByteCount(body);
            request.Method = (body == string.Empty) ? "GET" : "POST";
            if (body.Length > 0)
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(Encoding.UTF8.GetBytes(body), 0, (int)request.ContentLength);
                }
            }

            try
            {
                return (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {

                return (HttpWebResponse)e.Response; 
                // по уму бы  что-то такое наверн
                //return ((HttpWebResponse) e.Response).StatusCode;
            
            }
            
        }
    }
}
