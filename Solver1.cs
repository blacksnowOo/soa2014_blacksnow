﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Labs
{
    class Solver1
    {
        public void solve()
        {
            var typeOfSerialization = Console.ReadLine();
            var inputSerialized = Console.ReadLine();
            //todo: спрятать проверку на тип сериализации Json, Xml внутрь интерфейса
            
            var serialize = new HiddenVerification().hiddenVerification(typeOfSerialization);
            
            var input = serialize.deserialization<Input>(inputSerialized);
            
            var output = new Calculator().GenerateOutput(input);

            Console.WriteLine(serialize.serialization(output));
        }
        //todo: я имел ввиде внутрь интерфейса ISerialization
    }
}
