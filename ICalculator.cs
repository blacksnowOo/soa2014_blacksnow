﻿namespace Labs
{
    interface ICalculator
    {
        Output GenerateOutput(Input input);
    }
}
