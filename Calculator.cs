﻿using System.Linq;

namespace Labs
{
    class Calculator : ICalculator
    {
        public Output GenerateOutput(Input input)
        {
            Output output = new Output();
            output.SumResult = input.Sums.Sum() * input.K;

            
            output.MulResult = input.Muls.Aggregate((cur, next) => cur * next);

            output.SortedInputs = input.Sums.Concat(input.Muls.Select(element => (decimal)element)).ToArray();
            
            output.SortedInputs = output.SortedInputs.OrderBy(element => element).ToArray();

            return output;
        }
    }
}
